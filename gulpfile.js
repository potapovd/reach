const gulp = require("gulp"),

    sass = require("gulp-sass"),
    cleanCSS = require("gulp-clean-css"),
    autoprefixer = require("gulp-autoprefixer"),
    shorthand = require("gulp-shorthand"),

    uglify = require("gulp-uglify"),
    inject = require("gulp-inject"),

    htmlmin = require("gulp-htmlmin"),
    sourcemaps = require("gulp-sourcemaps"),

    cache = require("gulp-cache"),
    rigger = require("gulp-rigger"),
    rimraf = require("rimraf"),
    rename = require("gulp-rename"),
    notify = require("gulp-notify"),
    log = require("fancy-log"),
    plumber = require("gulp-plumber"),
    size = require("gulp-size"),

    htmlValidator = require("gulp-w3c-html-validator"),
    browserSync = require("browser-sync").create(),
    imagemin = require("gulp-imagemin"),
    ftp = require("vinyl-ftp");

const path = {
    build: {
        php: "build/",
        html: "build/",
        js: "build/js/",
        css: "build/css/",
        img: "build/img/",
        fonts: "build/fonts/",
        maps: "build/maps/",
        dir: "build/*"
    },
    src: {
        html: "src/*.html",
        php: "src/*.php",
        js: "src/js/scripts.js",
        style: "src/scss/style.scss",
        img: "src/img/**/*.*",
        fonts: "src/fonts/**/*.*"
    },
    watch: {
        php: "src/*.php",
        html: "src/*.html",
        js: "src/js/**/*.js",
        style: "src/scss/**/*.scss",
        img: "src/img/**/*.*",
        fonts: "src/fonts/**/*.*"
    },
    clean: "./build/**/*.*"
};

/*HELPERS*/
//HTML VALIDATION
gulp.task("html:validate", function () {
    log("___HTML:VALIDATION STARTED___");
    return gulp.src(path.src.html)
        .pipe(plumber())
        .pipe(htmlValidator())
        .pipe(htmlValidator.reporter())
});

//INJECTING CSS and JS
gulp.task("html:inject", function () {
    log("___HTML:INJECT STARTED___");
    return gulp.src("./build/*.html")
        .pipe(inject(gulp.src([ "./build/**/*.js", "./build/**/*.css" ], {
            read: false
        }), {
            relative: true
        }))
        .pipe(gulp.dest(path.build.html));
})

//CLEAN
gulp.task("clean", gulp.series(function () {
    log("___CLEANING STARTED___");
    return cache.clearAll();
}, function (cb) {
    return rimraf(path.clean, cb);
}));


/*TASKS*/
//FONTS
gulp.task("fonts:build", function () {
    log("___FONTS STARTED___");
    return gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});

// STYLES
gulp.task("style:build:dev", function () {
    log("___STYLES:DEV STARTED___");
    return gulp.src(path.src.style)
        .pipe(
            sass().on("error", notify.onError({
                title: "Sass Error",
                subtitle: [
                    "<%= error.relativePath %>",
                    "<%= error.line %>"
                ].join(":"),
                message: "<%= error.messageOriginal %>",
                open: "file://<%= error.file %>",
                onLast: true
            }))
        )
        .pipe(sourcemaps.init())
        .pipe(autoprefixer({compatibility: "*"}))
        .pipe(sourcemaps.write("."))
        .pipe(size({
            title: "CSS: ",
            showFiles: true
        })).on("error", err => {
            log.error(err.message);
        })
        .pipe(gulp.dest(path.build.css))
});
gulp.task("style:build:live", function () {
    log("___STYLES:LIVE STARTED___");
    return gulp.src(path.src.style)
        .pipe(
            sass().on("error", notify.onError({
                title: "Sass Error",
                subtitle: [
                    "<%= error.relativePath %>",
                    "<%= error.line %>"
                ].join(":"),
                message: "<%= error.messageOriginal %>",
                open: "file://<%= error.file %>",
                onLast: true
            }))
        )
        .pipe(sourcemaps.init())
        .pipe(size({
            title: "CSS BEFORE: ",
            showFiles: true
        }))
        .pipe(autoprefixer({cascade: false, compatibility: "*"}))
        .pipe(shorthand())
        .pipe(cleanCSS({debug: true}))
        .pipe(sourcemaps.write("/"))
        .pipe(rename({
            suffix: ".min",
            prefix: ""
        }))
        .pipe(size({
            title: "CSS AFTER: ",
            showFiles: true
        }))
        .pipe(gulp.dest(path.build.css))
});

//JS
gulp.task("js:build", function () {
    log("___JS STARTED___");
    return gulp.src(path.src.js)
        .pipe(rigger())
        .pipe(size({
            title: "JS BEFORE: ",
            showFiles: true
        }))
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(sourcemaps.write("."))
        .pipe(size({
            title: "JS AFTER:  ",
            showFiles: true
        })).on("error", err => {
            log.error(err.message);
        })
        .pipe(gulp.dest(path.build.js))

});

//HTML
gulp.task("html:build:dev", gulp.series(function () {
    log("___HTML:DEV STARTED___");
    return gulp.src(path.src.html)
        .pipe(plumber())
        .pipe(rigger())
        .pipe(size({
            title: " ",
            showFiles: true
        }))
        .pipe(gulp.dest(path.build.html));
}, "html:inject"));
gulp.task("html:build:live", gulp.series(function () {
    log("___HTML:LIVE STARTED___");
    return gulp.src(path.src.html)
        .pipe(plumber())
        .pipe(rigger())
        .pipe(size({
            title: "HTML BEFORE: ",
            showFiles: true
        }))
        .pipe(htmlmin({
            collapseWhitespace: true,
            useShortDoctype: true,
            minifyCSS: true,
            minifyJS: true
        }))
        .pipe(size({
            title: "HTML AFTER: ",
            showFiles: true
        }))
        .pipe(gulp.dest(path.build.html))
}, "html:inject"));

//PHP
gulp.task("php:build", function () {
    log("___PHP:BUILD STARTED___");
    return gulp.src(path.src.php)
        .pipe(rigger())
        .pipe(size({
            title: "PHP BEFORE: ",
            showFiles: true
        }))
        .pipe(htmlmin({
            collapseWhitespace: true,
            useShortDoctype: true,
            minifyCSS: true,
            minifyJS: true
        }))
        .pipe(size({
            title: "PHP AFTER: ",
            showFiles: true
        }))
        .pipe(gulp.dest(path.build.html))
});

//IMAGES
gulp.task("images:build", gulp.series(function () {
    log("___IMAGES:BUILD STARTED___");
    return gulp.src(path.src.img)
        .pipe(cache(imagemin([], {
            verbose: true,
            progressive: true
        })))
        .pipe(gulp.dest(path.build.img)) //И бросим в build
}));

//BUILDERS
gulp.task("build:live", gulp.series(
    "clean",
    "fonts:build",
    "style:build:live",
    "js:build",
    "html:build:live",
    "php:build",
    "images:build",
    function () {
        return gulp.src(
            "src/.htaccess",
            {dot: true}
        ).pipe(gulp.dest(path.build.html));
    },
));

gulp.task("build:dev", gulp.series(
    "clean",
    "fonts:build",
    "style:build:dev",
    "js:build",
    "php:build",
    "images:build",
    "html:build:dev",
));

//WATCHERS
gulp.task("watch:build:serve", gulp.series("build:dev",
    function () {
        notify("Watching for changes...").write("");

        browserSync.init({
            server: {
                baseDir: "./build"
            },
            notify: false
        });
        browserSync.watch("build/").on("change", browserSync.reload);

        gulp.watch([ path.watch.fonts ], gulp.series("fonts:build"));
        gulp.watch([ path.watch.style ], gulp.series("style:build:dev"));
        gulp.watch([ path.watch.js ], gulp.series("js:build"));
        gulp.watch([ path.watch.img ], gulp.series("images:build"));
        gulp.watch([ path.watch.php ], gulp.series("php:build"));
        gulp.watch([ path.watch.html ], gulp.series("html:build:dev"));
    }
));

gulp.task("watch", function (done) {
    notify("Watching for changes...").write("");
    gulp.watch([ path.watch.fonts ], gulp.series("fonts:build"));
    gulp.watch([ path.watch.style ], gulp.series("style:build:dev"));
    gulp.watch([ path.watch.js ], gulp.series("js:build"));
    gulp.watch([ path.watch.img ], gulp.series("images:build"));
    gulp.watch([ path.watch.php ], gulp.series("php:build"));
    gulp.watch([ path.watch.html ], gulp.series("html:build:dev"));
    done();
})