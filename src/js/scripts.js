/* CUSTOM FONTS https://www.bramstein.com/writing/web-font-loading-patterns.html */
//= plugins/fontfaceobserver.min.js
var fontOneLoad = new FontFaceObserver("Bebas Neue Cyrillic", {});
var fontOneLoadDin = new FontFaceObserver("DIN", {});
var fontOneLoadDinCon = new FontFaceObserver("DIN Con", {});
fontOneLoad.load().then(function () {
    sessionStorage.fontsLoaded = true;
    var html = document.documentElement;
    html.classList.add("fonts-loaded");
}).catch(function () {
    sessionStorage.fontsLoaded = false;
    console.log("font  Bebas Neue Cyrillic !Loaded")
});
fontOneLoadDin.load().then(function () {
    sessionStorage.fontsLoaded = true;
    var html = document.documentElement;
    html.classList.add("fonts-loaded");
}).catch(function () {
    sessionStorage.fontsLoaded = false;
});
fontOneLoadDinCon.load().then(function () {
    sessionStorage.fontsLoaded = true;
    var html = document.documentElement;
    html.classList.add("fonts-loaded");
}).catch(function () {
    sessionStorage.fontsLoaded = false;
});
if ( sessionStorage.fontsLoaded ) {
    var html = document.documentElement;
    html.classList.add("fonts-loaded");
}



cackle_widget = window.cackle_widget || [];
cackle_widget.push({ widget: "Review", id: 72137 });
(function () {
    var mc = document.createElement("script");
    mc.type = "text/javascript";
    mc.async = true;
    mc.src = ("https:" == document.location.protocol ? "https" : "http") + "://cackle.me/widget.js";
    var s = document.getElementsByTagName("script")[0];
    s.parentNode.insertBefore(mc, s.nextSibling);
})();

/* CUSTOM SCRIPTS LOADER */
function addScript(src) {
    var script = document.createElement("script");
    script.src = src;
    script.type = "text/javascript";
    script.async = false;
    document.head.appendChild(script);
}

/* https://github.com/alexfoxy/laxxx */
//= plugins/lax-1.2.5.min.js
//= plugins/aos-2.3.4.min.js
//= plugins/swiper/swiper.min.js

function distanceParser(distance) {

    var response = [];
    response["days"] = Math.floor(distance / (1000 * 60 * 60 * 24));
    response["hours"] = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    response["minutes"] = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    response["seconds"] = Math.floor((distance % (1000 * 60)) / 1000);
    return response;
}

/* jQuery */
//= plugins/jquery-3.5.0.min.js

$(document).ready(function () {

    //animation AOS
    AOS.init({
        once: false,
        mirror: true,
    });
    //animation LAX
    window.onload = function () {
        lax.setup() // init
        var updateLax = function () {
            lax.update(window.scrollY)
            window.requestAnimationFrame(updateLax)
        }
        window.requestAnimationFrame(updateLax)
    }

    // window.onload = function () {
    //     if ( window.jQuery ) {
    //         // jQuery is loaded
    //         //alert("Yeah!");
    //     } else {
    //         // jQuery is not loaded
    //         //alert("Doesn't Work");
    //     }
    // }

    $("#full-text").on("click", function () {
        $(".show-full-text").slideToggle("slow")
        return false
    });

    var mySwiper = new Swiper("#swiper-container-goals", {
        slidesPerView: 4,
        centeredSlides: true,
        //slidesPerView: 'auto',
        spaceBetween: 10,
        loop: true,
        scrollbar: {
            el: ".swiper-scrollbar",
            //hide: true,
        },
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
        breakpoints: {
            320: {
                //slidesPerView: 1,
                slidesPerView: 1,
                spaceBetween: 20,
            },
            767: {
                //slidesPerView: 2,
                slidesPerView: 1,
                spaceBetween: 20,
            },
            991: {
                slidesPerView: 2,
                spaceBetween: 10,
            },
            1280: {
                //slidesPerView: 'auto',
                slidesPerView: 3,
                spaceBetween: 20,
            }
        }
    });

    var mySwiper = new Swiper("#swiper-container-if", {
        slidesPerView: 1,
        //centeredSlides: true,
        //slidesPerView: 'auto',
        spaceBetween: 40,
        loop: true,
        pagination: {
            el: ".swiper-pagination",
        },
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
            hide: true,
        },

    });

    //Countdown
    var countDownDate = new Date("May 30, 2020 00:00:00").getTime();

    //today countdowm
    var todayCounter = new Date();
    todayCounter.setHours(23, 59, 59, 0);
    console.log(todayCounter);
    if ( todayCounter < countDownDate ) {
        var total = $("#total-price")
        var x = setInterval(function () {
            var now = new Date().getTime();
            var distance = todayCounter - now;
            var response = distanceParser(distance);

            document.getElementById("countdown").innerHTML =
                response["hours"] + "ч.:" + response["minutes"] + "м.:" + response["seconds"] + "c ";

            if ( distance < 0 ) {
                clearInterval(x);
                $("#countdown, #action-price").hide()
                // $("#total-price").html("10000")
                //document.getElementById("countdown").innerHTML = "EXPIRED";
            }
        }, 1000)
    } else {
        $("#countdown, #offer-price").hide()
        $("#action-price .line-through").hide()
    }


    //today amount
    var now = new Date().getTime();
    var distance = countDownDate - now;
    var responseDist = distanceParser(distance);
    var d = 0;
    console.log(responseDist);
    if ( responseDist["days"] > 0 ) {
        if ( responseDist["days"] <= 4 ) {
            d = (5 - responseDist["days"] - 1) * 100;
            d += 3500
        } else {
            d = 3500
        }
    } else if ( responseDist["days"] == 0 ) {
        d = 3900
    } else {
        d = 3900
    }

    total.html(d)






    //addScript("https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU");
});