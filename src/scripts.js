/* CUSTOM FONTS https://www.bramstein.com/writing/web-font-loading-patterns.html */
//= plugins/fontfaceobserver.min.js
var fontOneLoad = new FontFaceObserver("", {});
fontOneLoad.load().then(function () {
	sessionStorage.fontsLoaded = true;
}).catch(function () {
	sessionStorage.fontsLoaded = false;
});
if (sessionStorage.fontsLoaded) {
	var html = document.documentElement;
	html.classList.add('fonts-loaded');
}


/* CUSTOM SCRIPTS LOADER */
function addScript(src) {
	var script = document.createElement("script");
	script.src = src;
	script.type = "text/javascript";
	script.async = false;
	document.head.appendChild(script);
}

/* jQuery */
//= plugins/jquery-3.5.0.slim.min.js
$(document).ready(function () {

    window.onload = function () {
        if ( window.jQuery ) {
            // jQuery is loaded
            //alert("Yeah!");
        } else {
            // jQuery is not loaded
            //alert("Doesn't Work");
        }
    }

    //addScript("https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU");
});